#!/bin/sh -e

PREFIX=$HOME/.local
NUM_CORES=$(nproc --all)
TOMATO_SAUCE_DIR=${HOME}/.tomato-sauce
BUILD_DIR=${TOMATO_SAUCE_DIR}/build
CACHE_DIR=${TOMATO_SAUCE_DIR}/cache
PACKAGE_DIR=${TOMATO_SAUCE_DIR}/packages
INSTALLED_PACKAGES_DIR=${TOMATO_SAUCE_DIR}/installed-packages

# Create the $PREFIX directory if it does not yet exist
mkdir -p $PREFIX

install() {
  extension="${url##*.}"
  full_package_name=${package}-${version}
  base_filename="${full_package_name}.tar.${extension}"
  cache_file=${CACHE_DIR}/${base_filename}

  if [ -e ${INSTALLED_PACKAGES_DIR}/${full_package_name} ]; then
    printf "Not installing $s as it is already installed\n" "${full_package_name}"
    return
  fi

  for dep in ${dependencies}; do
    printf "Installing %s as a dependency of %s\n" "${dep}" "${package}"
    install-package ${dep}
  done

  if [ ! -e ${cache_file} ]; then
    download_file "${url}" "${cache_file}"
  else
    printf "Skipping download as package is already downloaded at %s" "${cache_file}"
  fi
   

  package_build_directory="${BUILD_DIR}/${full_package_name}"
  mkdir -p ${package_build_directory}

  extract_tar "${cache_file}" "${package_build_directory}"
  (export_flags && cd "${BUILD_DIR}/${full_package_name}" && build)

  touch ${INSTALLED_PACKAGES_DIR}/${full_package_name}
}

download_file() {
  local url="${1}"
  local destination="${2}"

  printf "Downloading file from url: %s to %s\n" "${url}" "${destination}"

  curl "${url}" \
    --location \
    --insecure \
    --create-dirs \
    --progress-bar \
    --output "${destination}"
}

extract_tar() {
  local filename="${1}"
  local directory="${2}"

  printf "Extracting file %s to %s\n" "${filename}" "${directory}"

  tar --file "${filename}" \
    --extract "--$(get_tar_option "${filename}")" \
    --directory="${directory}" --strip-components=1
}

get_tar_option() {
  local filename="${1}"
  case "${filename##*.}" in
  gz)
    printf 'gzip'
    ;;
  xz)
    printf 'xz'
    ;;
  bz2)
    printf 'bzip2'
    ;;
  esac
}

export_flags(){
  export PATH="${PREFIX}/bin:${PATH}"
  export LIBRARY_PATH="${PREFIX}/lib:${LIBRARY_PATH}"
  export CFLAGS="-I${PREFIX}/include -fPIC ${CFLAGS}"
  export CPPFLAGS="-I${PREFIX}/include ${CPPFLAGS}"
  export CXXFLAGS="-I${PREFIX}/include -fPIC ${CXXFLAGS}"
  export LDFLAGS="-L${PREFIX}/lib ${LDFLAGS}"
}

package="${1}"

if [ ! -e "${PACKAGE_DIR}/${package}.sh" ]; then
  printf "No package named '%s' found in %s\n" "${package}" "${PACKAGE_DIR}"
  exit 1
fi

# Load definitions
. "${PACKAGE_DIR}/${package}.sh"

install "${package}" "${version}"
