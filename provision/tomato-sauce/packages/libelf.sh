#!/bin/sh

name='libelf'
version='0.8.13'
url="http://www.mr511.de/software/${name}-${version}.tar.gz"
dependencies=''

build() {
  ./configure --prefix="${PREFIX}" \
    --disable-shared \
    --enable-static

  make --jobs ${NUM_CORES}
  make install
}
