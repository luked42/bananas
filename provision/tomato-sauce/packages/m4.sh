#!/bin/sh

name='m4'
version='1.4.18'
url="https://ftp.gnu.org/gnu/${name}/${name}-${version}.tar.gz"
dependencies=''

build() {
  ./configure --prefix="${PREFIX}" 

  make --jobs ${NUM_CORES}
  make install
}
