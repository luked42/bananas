#!/bin/sh

name='vim'
version='8.0.0045'
url="https://github.com/${name}/${name}/archive/v${version}.tar.gz"
dependencies='ncurses python'

build() {
  ./configure --prefix="${PREFIX}" \
    --with-features=huge \
    --enable-multibyte \
    --enable-cscope \
    --enable-rubyinterp \
    --enable-pythoninterp \
    --with-python-config-dir="${PREFIX}/bin/python2.7-config" \
    --enable-luainterp \
    --disable-darwin \
    --disable-gui \
    --disable-netbeans \
    --without-x \
    --with-tlib=tinfow
    

  make --jobs ${NUM_CORES}
  make install
}
