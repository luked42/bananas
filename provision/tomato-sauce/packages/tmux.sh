#!/bin/sh

name='tmux'
version='2.5'
url="https://github.com/tmux/tmux/releases/download/${version}/${name}-${version}.tar.gz"
dependencies='libevent ncurses'

build() {
  
  export CFLAGS="-I${PREFIX}/include"
  export LDFLAGS="-L${PREFIX}/lib -L${PREFIX}/include"
  export CPPFLAGS="-I${PREFIX}/include"
  export LIBNCURSES_CLAGS="-D_GNU_SOURCE"
  export LIBNCURSES_LIBS="-lncursesw -ltinfow"
  export PKG_CONFIG_PATH="${PREFIX}/share/pkgconfig"

  ./configure --prefix="${PREFIX}" 

  make --jobs ${NUM_CORES}
  make install
    
}
