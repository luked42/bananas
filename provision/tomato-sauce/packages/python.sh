#!/bin/sh

name='Python'
version='2.7.3'
url="http://www.python.org/ftp/python/${version}/${name}-${version}.tar.xz"
dependencies=''

build() {
  ./configure --prefix="${PREFIX}" --enable-shared

  make --jobs ${NUM_CORES}
  make install
}
