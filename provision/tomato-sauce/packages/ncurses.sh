#!/bin/sh

name='ncurses'
version='6.0'
url="https://ftp.gnu.org/gnu/${name}/${name}-${version}.tar.gz"
dependencies=''

build() {
  ./configure --prefix="${PREFIX}" \
    --with-static \
    --without-shared \
    --with-termlib \
    --with-ticlib \
    --without-ada \
    --without-debug \
    --without-progs \
    --without-tests \
    --enable-const \
    --enable-ext-colors \
    --enable-overwrite \
    --enable-pc-files \
    --enable-sigwinch \
    --enable-wgetch-events \
    --enable-widec \
    --with-pkg-config-libdir="${PREFIX}/share/pkgconfig"

  make --jobs ${NUM_CORES}
  make install
}
