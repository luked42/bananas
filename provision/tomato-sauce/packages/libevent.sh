#!/bin/sh

name='libevent'
version='2.1.8-stable'
url="https://github.com/libevent/libevent/releases/download/release-${version}/${name}-${version}.tar.gz"
dependencies=''

build() {
  ./configure --prefix="${PREFIX}" \
    --enable-static \
    --disable-shared

  make --jobs ${NUM_CORES}
  make install
}
