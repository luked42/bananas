#!/bin/sh

name='gcc'
version='7.1.0'
url="https://ftp.gnu.org/pub/gnu/gcc/gcc-${version}/gcc-${version}.tar.gz"
dependencies='gmp mpfr mpc libelf'

build() {
  build_dir=$(pwd)
  mkdir -p ${build_dir}/build
  cd build

  unset C_INCLUDE_PATH
  unset CPLUS_INCLUDE_PATH
  #unset CFLAGS
  unset CPPFLAGS
  #unset CXXFLAGS
  unset LIBRARY_PATH
  unset LDFLAGS

  ${build_dir}/configure --prefix="${PREFIX}" \
    --disable-shared \
    --enable-static \
    --disable-libada \
    --disable-multilib \
    --enable-lanaguages=c,c++ \
    --with-gmp=${PREFIX} \
    --with-mpfr=${PREFIX} \
    --with-mpc=${PREFIX}  \
    --with-libelf=${PREFIX}

  make --jobs ${NUM_CORES}
  make install

  cd ${build_dir}
}
