#!/bin/sh -x

name='mpc'
version='1.0.3'
url="https://ftp.gnu.org/gnu/${name}/${name}-${version}.tar.gz"
dependencies='gmp mpfr'

build() {
  ./configure --prefix="${PREFIX}" --disable-shared --enable-static --with-gmp="${PREFIX}" --with-mpfr="${PREFIX}"

  make --jobs ${NUM_CORES}
  make install
}
