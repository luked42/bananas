#!/bin/sh

name='cmake'
version='3.9'
url="https://cmake.org/files/v${version}/${name}-${version}.0.tar.gz"
dependencies=''

build() {
  ./bootstrap --prefix="${PREFIX}"

  make --jobs ${NUM_CORES}
  make install
}
