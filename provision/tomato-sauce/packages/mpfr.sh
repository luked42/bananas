#!/bin/sh

name='mpfr'
version='3.1.5'
url="https://ftp.gnu.org/gnu/${name}/${name}-${version}.tar.gz"
dependencies='gmp'

build() {
  ./configure --prefix="${PREFIX}" \
    --disable-shared \
    --enable-static \
    --with-gmp="${PREFIX}/"

  make --jobs ${NUM_CORES}
  make install
}
