#!/bin/sh

name='gmp'
version='6.1.2'
url="https://ftp.gnu.org/gnu/${name}/${name}-${version}.tar.xz"
dependencies='m4'

build() {
  ./configure --prefix="${PREFIX}" \
    --disable-shared \
    --enable-static

  make --jobs ${NUM_CORES}
  make install
}
