# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

export PATH="${HOME}/.local/bin:${HOME}/.local/lib/:${PATH}"

export LD_LIBRARY_PATH="${HOME}/.local/bin/:${HOME}/.local/lib/:${LD_LIBRARY_PATH}"
