PROVISION_DIR=/vagrant/provision

# Copy out tomato sauce
cp -r ${PROVISION_DIR}/tomato-sauce ${HOME}/.tomato-sauce

# Set up local directories
mkdir -p ${HOME}/.local/bin

ln -fs ${HOME}/.tomato-sauce/install-package.sh ${HOME}/.local/bin/install-package

# Copy out the bashrc
cp ${PROVISION_DIR}/.bashrc ${HOME}/.bashrc

# We need a c-compiler
sudo yum -y groupinstall "Development tools"

